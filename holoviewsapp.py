import pandas as pd
import holoviews as hv
import param
import panel as pn
import numpy as np

from holoviews.operation.timeseries import rolling, RollingBase
from holoviews.operation.datashader import datashade, shade, dynspread, spread, rasterize
from holoviews.operation import decimate
hv.extension("bokeh")
from holoviews import Element, Operation
from holoviews.core.data import PandasInterface

class TimeRolling(Operation, RollingBase):
    """
    Applies a function over a rolling window.
    """

    window_type = param.ObjectSelector(default=None, allow_None=True,
                                       objects=['boxcar', 'triang', 'blackman', 'hamming', 'bartlett',
                                                'parzen', 'bohman', 'blackmanharris', 'nuttall',
                                                'barthann', 'kaiser', 'gaussian', 'general_gaussian',
                                                'slepian'], doc="The shape of the window to apply")

    function = param.Callable(default=np.mean, doc="""
        The function to apply over the rolling window.""")

    def _process_layer(self, element, key=None):
        xdim = element.kdims[0].name
        df = PandasInterface.as_dframe(element)
        df_kwargs = self._roll_kwargs()
        df_kwargs['window'] = str(df_kwargs['window']) + 'S'
        df_kwargs.pop('center')
        df = df.set_index(xdim).rolling(win_type=self.p.window_type,
                                        **df_kwargs)
        if self.p.window_type is None:
            kwargs = {'raw': True}
            rolled = df.apply(self.p.function, **kwargs)
        else:
            if self.p.function is np.mean:
                rolled = df.mean()
            elif self.p.function is np.sum:
                rolled = df.sum()
            else:
                raise ValueError("Rolling window function only supports "
                                 "mean and sum when custom window_type is supplied")
        return element.clone(rolled.reset_index())

    def _process(self, element, key=None):
        return element.map(self._process_layer, Element)

LITERS_PER_FRAME = 2.32
FRAMES_PER_SECOND = 14

df = pd.read_csv('data/data.csv')
TAXA = sorted(df['taxon'].unique())
df['time'] = (df.LCM_event_timestamp * 1000).astype('datetime64[ns]')
df = df[df['time'].between(np.datetime64('2020-03-15 21:30:00.000'), np.datetime64('2020-03-16 00:40:39.000'))]

time_bounds = (np.datetime64(df['time'].min()), np.datetime64(df['time'].max()))

confidence = pn.widgets.FloatSlider(name="Confidence", value=0.5, start=0, end=1, step=0.01)
window = pn.widgets.IntSlider(name="Window size (seconds)", value=10, start=1, end=600, step=1)
dates = pn.widgets.DateRangeSlider(name="Date Range", start=time_bounds[0], end=time_bounds[1])
taxon = pn.widgets.Select(name="Taxon", options=TAXA)

@pn.depends(confidence=confidence.param.value,
            window=window.param.value,
            dates=dates.param.value,
            taxon=taxon.param.value)
def taxon_concentration(confidence, window, dates, taxon):
    taxon_selector = df.taxon == taxon
    dates_selector = df.time.between(*dates)
    confi_selector = df.confidence >= confidence
    data = df[taxon_selector & dates_selector & confi_selector][['time', 'DEPTH_M']]
    data = data[['time', 'DEPTH_M']].value_counts().rename('concentration').rename_axis(['time', 'depth']).div(LITERS_PER_FRAME).reset_index('depth').sort_index()
    data = data.rolling(window=str(window) + 's').apply(np.mean)
    data['Log concentration'] = data['concentration'].apply(np.log10)

    table = hv.Table(data, kdims=['time', 'depth'], vdims=['Log concentration'])
    points = hv.Points(table).opts(show_grid=True, width=1200, colorbar=True, cmap='gist_rainbow', framewise=True, tools=['hover'], invert_yaxis=True, logy=True, color='Log concentration')
    return points.opts(title=f'Log concentration ({taxon})')

@pn.depends(confidence=confidence.param.value,
            window=window.param.value,
            dates=dates.param.value)
def stackplot(confidence, window, dates):
    dates_selector = df.time.between(*dates)
    confi_selector = df.confidence >= confidence
    data = df[dates_selector & confi_selector]
    data = data[['time', 'taxon']].value_counts().rename('concentration').rename_axis(['time', 'taxon']).div(LITERS_PER_FRAME).reset_index()
    data['fraction'] = data['concentration'] / data.groupby(['time'])['concentration'].transform(sum)
    areas = {taxon: (hv.Area(data[data.taxon == taxon], kdims=['time'], vdims=['fraction'])) for taxon in TAXA}
    overlay = hv.NdOverlay(areas).opts(legend_position='right')
    stack = hv.Area.stack(overlay).opts(framewise=True, width=1200, height=600)
    smooth = TimeRolling(stack, rolling_window=window).opts(title = 'Taxon fraction vs. Time')
    return smooth

@pn.depends(confidence=confidence.param.value,
            window=window.param.value,
            dates=dates.param.value,
            taxon=taxon.param.value)
def taxon_concentration(confidence, window, dates, taxon):
    taxon_selector = df.taxon == taxon
    dates_selector = df.time.between(*dates)
    confi_selector = df.confidence >= confidence
    data = df[taxon_selector & dates_selector & confi_selector][['time', 'DEPTH_M']]
    data = data[['time', 'DEPTH_M']].value_counts().rename('concentration').rename_axis(['time', 'depth']).div(LITERS_PER_FRAME).reset_index('depth').sort_index()
    data = data.rolling(window=str(window) + 's').apply(np.mean)
    data['Log concentration'] = data['concentration'].apply(np.log10)
    table = hv.Table(data, kdims=['time', 'depth'], vdims=['Log concentration'])
    points = hv.Points(table).opts(show_grid=True, width=1200, colorbar=True, cmap='gist_rainbow', framewise=True, tools=['hover'], invert_yaxis=True, logy=True, color='Log concentration')
    return points.opts(title=f'Log concentration ({taxon})')



def main():
    widgets = pn.WidgetBox('Parameters', confidence, dates, window, taxon)
    dmaps = [hv.DynamicMap(stackplot), hv.DynamicMap(taxon_concentration)]
    rows = [widgets, *dmaps]
    col = pn.Column(*rows)
    pn.serve(col,
             port=5006,
             allow_websocket_origin=["127.0.0.1:5000", "127.0.0.1:5600", "localhost:5000", "localhost:5600"],
             show=False,
             )

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
