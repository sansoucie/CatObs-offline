#!/usr/bin/env bash

source ./venv/bin/activate

trap 'jobs -p | xargs -r kill && kill -9 $PPID' SIGINT SIGTERM EXIT

python holoviewsapp.py &
python flaskapp.py